#!/bin/sh

ARCH=$1

if [ -z "$ARCH" ]; then
  echo "Please specify the docker build arch"
  exit 0
fi

if [ "$DEBUG" = "true" ]; then
  set -ex
else
  set -e
fi

./makesquashfs.sh $ARCH

cpio_file=$(mktemp /tmp/magiccpio.XXXXXXX)
find btools | cpio -o -H newc > $cpio_file
xz --keep --check=crc32 --lzma2=dict=512KiB $cpio_file
fl=$(stat -c %s $cpio_file.xz)
rem=$(($fl % 4))
fb= ;
if [ $rem = 0 ]; then
  fb=$fl
else
  fb=$(($fl + 4 - $rem))
fi;

dd if=/dev/zero of=$cpio_file.xz4 bs=1 count=$fb
dd if=$cpio_file.xz of=$cpio_file.xz4 conv=notrunc

mv $cpio_file.xz4 ./tpm-aca.$ARCH.cpio.gz4

rm /tmp/magiccpio.*