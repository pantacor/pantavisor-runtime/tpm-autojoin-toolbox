#!/bin/sh

ARCH=$1

if [ -z "$ARCH" ]; then
  echo "Please specify the docker build arch"
  exit 0
fi

if [ "$DEBUG" = "true" ]; then
  set -ex
else
  set -e
fi


curdir=`pwd`

dir=`mktemp -d -t XXXX.ext-tpm-aca.dir`

docker build . -f "Dockerfile.$ARCH" -t pvf-aca-toolbox:$ARCH

sh -c "cd $dir; pvr init; fakeroot pvr app add --from=pvf-aca-toolbox:$ARCH test"

mv -f $dir/test/root.squashfs $curdir/btools/toolbox.ext-tpm-aca.squashfs

rm -rf $dir